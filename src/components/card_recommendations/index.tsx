export default function CardRecommendations() {
    return (
        <div className="card w-full bg-base-100 shadow-xl">
            <div className="card-body">
                <h2 className="card-title">Recomendaciones Generales</h2>
                <table className="table text-normal">
                    <tbody >
                        <tr>
                            <th className="font-normal">Prefiera calefactores no contaminantes.</th>
                        </tr>
                        <tr>
                            <th className="font-normal">Realice mantención a su calefactor con la periodicidad indicada por el fabricante.</th>
                        </tr>
                        <tr>
                            <th className="font-normal">Prefiera el transporte público y/o comparta su auto.</th>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    )
}