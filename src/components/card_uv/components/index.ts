import ModalInformation from "./modal_information";
import ModalExtraInfo from "./modal_extra_info";

export { ModalInformation, ModalExtraInfo };