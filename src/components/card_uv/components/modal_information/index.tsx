'use client'
import { faCircleInfo } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";


export default function ModalInformation() {
    const [modalIsOpen, setmodalIsOpen] = useState(false);

    const openModal = () => setmodalIsOpen(true);
    const closeModal = () => setmodalIsOpen(false);

    return (
        <>
            <button className={'flex items-center text-xs justify-center'} onClick={openModal}>
                Mas información
                <FontAwesomeIcon icon={faCircleInfo} className="max-h-[15px] ml-2" />
            </button>
            <dialog className={`modal ${modalIsOpen && 'modal-open'}`}>
                <form method="dialog" className="modal-box">
                    <h3 className="font-bold text-lg">Información Útil</h3>
                    <div className="overflow-x-auto">
                        <table className="table">
                            <tbody>
                                <tr>
                                    <td>
                                        Utilizar protector solar factor 40 FPS (como mínimo), el cual debe ser aplicado cada dos horas de exposición en rostro, cuello y todas las extremidades expuestas.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Viste ropa de manga larga, pantalones largos y un sombrero de ala ancha para proteger tu piel del sol. Elige telas de colores oscuros y con una trama apretada, ya que brindan una mejor protección.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Evita la exposición directa al sol, especialmente durante las horas pico de radiación ultravioleta, que suelen ser entre las 10 a.m. y las 4 p.m. Siempre que sea posible, busca sombra, ya sea bajo árboles, sombrillas o estructuras cubiertas.
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Incluso en días nublados, la radiación UV puede penetrar las nubes y dañar tu piel. Asegúrate de aplicar protector solar y seguir otras medidas de protección incluso cuando no haya sol directo.
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="modal-action">
                        <button className="btn" onClick={closeModal} >Close</button>
                    </div>
                </form>
            </dialog>
        </>
    )
}