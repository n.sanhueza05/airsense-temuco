'use client'
import { faCircleInfo } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";

export default function ModalExtraInfo() {
    const [modalIsOpen, setmodalIsOpen] = useState(false);

    const openModal = () => setmodalIsOpen(true);
    const closeModal = () => setmodalIsOpen(false);
    return (
        <>
            <h3 className="text-sm hover:cursor-pointer" onClick={openModal}>Más información sobre los estados de calidad
                <FontAwesomeIcon icon={faCircleInfo} className="max-h-[15px] ml-2" />
            </h3>
            <dialog className={`modal ${modalIsOpen && 'modal-open'}`}>
                <form method="dialog" className="modal-box">
                    <h3 className="font-bold text-lg">Información calidad aire</h3>
                    <p>
                        Según las mediciones proporcionadas por el Instituto de Seguridad Laboral, se establecen los diferentes niveles de índices de radiación ultravioleta son:
                    </p>
                    <div className="overflow-x-auto">
                        <table className="table">
                            <tbody>
                                <tr>
                                    <th>Nivel Bajo<span className="badge badge-xs bg-text-good"></span></th>
                                    <td>1-2 Índice UV </td>
                                </tr>
                                <tr>
                                    <th>Nivel Moderado<span className="badge badge-xs bg-text-regular"></span></th>
                                    <td>3-4-5 Índice UV</td>
                                </tr>
                                <tr>
                                    <th>Nivel Alto<span className="badge badge-xs bg-text-alert"></span></th>
                                    <td>6-7 Índice UV</td>
                                </tr>
                                <tr>
                                    <th>Nivel Muy Alto<span className="badge badge-xs bg-text-bad"></span></th>
                                    <td>8-9-10 Índice UV</td>
                                </tr>
                                <tr>
                                    <th>Nivel Extremadamente Alto<span className="badge badge-xs bg-text-emergency"></span></th>
                                    <td>11 Índice UV</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="modal-action">
                        <button className="btn" onClick={closeModal} >Close</button>
                    </div>
                </form>
            </dialog>
        </>
    )
}