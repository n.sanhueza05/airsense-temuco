import { faSun } from "@fortawesome/free-regular-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import selectStatusUV from "./status";
import { ModalExtraInfo, ModalInformation } from "./components";

interface Props {
    currentUV: number;
}

export default function CardUV(props: Props) {

    const selectedStatus = selectStatusUV(props.currentUV);

    return (
        <div className="card w-full bg-base-100 shadow-xl">
            <div className="card-body grid grid-cols-2 pb-16">
                <h2 className="card-title">Estado Actual</h2>
                <div className="h-full flex items-center justify-end">
                    <ModalInformation />
                </div>
                <div className="grid grid-cols-3 w-full col-span-2">
                    <div className="flex items-center">
                        <FontAwesomeIcon icon={faSun} color={selectedStatus.color} className="min-h-[100px] max-h-[100px]" />
                    </div>
                    <div className="flex items-center justify-center lg:justify-normal">
                        <div>
                            <h3 className="md:text-lg">Actual</h3>
                            <h1 className="md:text-xl text-lg font-bold">{props.currentUV}</h1>
                        </div>
                    </div>
                    <div className="flex items-center">
                        <div>
                            <h3 className="text-lg">Estado</h3>
                            <h1 className="md:text-xl text-lg font-bold">{selectedStatus.name}</h1>
                        </div>
                    </div>
                </div>
                <div className="absolute bottom-5">
                    <ModalExtraInfo />
                </div>
            </div>
        </div>
    )
}