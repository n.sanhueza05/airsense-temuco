
const statusesUV = [
    {
        name: `Bajo`, color: `#00A86B`,
    },
    {
        name: `Moderado`, color: `#FFC107`,
    },
    {
        name: `Alto`, color: `#FF5722`,
    },
    {
        name: `Muy Alto`, color: `#D32F2F`,
    },
    {
        name: `Extremadamente Alto`, color: `#9C27B0`,
    }
]



const selectStatusUV = (uv : number) => {
    if (uv < 3) return statusesUV[0]
    if (uv < 6) return statusesUV[1]
    if (uv < 8) return statusesUV[2]
    if (uv < 11) return statusesUV[3]
    return statusesUV[4]
}

export default selectStatusUV