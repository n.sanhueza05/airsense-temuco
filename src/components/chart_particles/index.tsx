'use client'
import ReactEcharts from 'echarts-for-react';

interface Props {
  dates: string[];
  pm2_5: number[];
  pm10: number[];
  uvs: number[];
}

export default function ChartParticles(props: Props) {
  const option = {
    visualMap: [
      {
        show: false,
        type: 'continuous',
        seriesIndex: 0,
        min: 0,
        max: 150,
      },
    ],
    title: [
      {
        left: 'center',
        text: 'Partículas MP 2.5/10 e Índice UV',
      },
    ],
    tooltip: {
      trigger: 'axis',
    },
    xAxis: [
      {
        data: props.dates,
      },
    ],
    yAxis: [{}],
    series: [
      {
        name: 'PM2.5',
        type: 'line',
        showSymbol: false,
        data: props.pm2_5,
      },
      {
        name: 'PM10',
        type: 'line',
        showSymbol: false,
        data: props.pm10,
      },
      {
        name: 'UV',
        type: 'line',
        showSymbol: false,
        data: props.uvs,
      }
    ],
  };

  return (
    <ReactEcharts option={option} className='min-h-full' />
  )
}
