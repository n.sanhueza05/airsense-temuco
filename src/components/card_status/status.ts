import { faFaceFrown, faFaceLaughBeam, faFaceMeh, faFaceSadTear, faFaceSmileBeam } from "@fortawesome/free-regular-svg-icons";

const status = [
    {
        icon: faFaceLaughBeam, status: 'Bueno', color: '#00A86B', textColor: 'text-text-good',
        recommendations: [
            'Disfrutar de las actividades al aire libre',
            'Aprovechar de ventilar el hogar',
            'Seguir las recomendaciones generales'
        ]
    },
    {
        icon: faFaceSmileBeam, status: 'Regular', color: '#FFC107', textColor: 'text-text-regular',
        recommendations: [
            'Evitar actividades intensas al aire libre',
            'De ser posible utilizar purificadores de aire dentro del hogar',
            'Seguir las recomendaciones generales'
        ]
    },
    {
        icon: faFaceMeh, status: 'Alerta', color: '#FF5722', textColor: 'text-text-alert',
        recommendations: [
            'Limitar actividades al aire libre',
            'De ser posible permanecer en interiores',
            'Cerrar ventanas y puertas',
            'Prohibición en TODO Temuco y Padre Las Casas del uso de más de un artefacto a leña por vivienda, entre las 18:00 y las 06:00 horas del día siguiente. (Verificar con la autoridad sanitaria)'
        ]
    },
    {
        icon: faFaceFrown, status: 'Preemergencia', color: '#D32F2F', textColor: 'text-text-bad',
        recommendations: [
            'Evitar la totalidad de actividades al aire libre',
            'Considerar el uso de mascarillas',
            'Cerrar ventanas y puertas',
            'Acatar las restricciones decretadas por la autoridad sanitaria que pueden ser las siguientes',
            'Se prohíbe el uso de artefactos a leña, entre las 18:00 y las 06:00 horas del día siguiente. ',
            'Se prohíbe el funcionamiento de calderas a leña con una potencia térmica nominal menor a 75 kWt entre las 18:00 y las 06:00 horas del día siguiente.',
            'Se prohíbe en TODO Temuco y Padre Las Casas, durante 24 hrs (desde las 06:00 AM del día que se pronostique episodio, hasta las 06:00 AM del día siguiente), el funcionamiento de calderas con una potencia mayor a 75 kWt que presenten emisiones mayores o iguales a 50 mg/m³N de material particulado.',
            'IMPORTANTE Las estufas a pellet no poseen restricción de uso.'
        ]
    },
    {
        icon: faFaceSadTear, status: 'Emergencia', color: '#9C27B0', textColor: 'text-text-emergency',
        recommendations: [
            'Permanecer en interiores',
            'Cerrar ventanas y puertas',
            'Utilizar aire acondicionado en modo recirculación de aire en conjunto con purificadores de aire',
            'Se recomienda el uso de mascarilla',
            'Estar atento a síntomas de problemas de salud',
            'Revisar las restricciones decretadas por la autoridad sanitaria que pueden ser las siguientes',
            'En TODO Temuco y Padre Las Casas se prohíbe el uso de artefactos a leña, durante 24 Horas (entre las 06:00 y las 06:00 hrs. del día siguiente).',
            'En TODO Temuco y Padre Las Casas se prohíbe el funcionamiento de calderas a leña con una potencia térmica nominal menor a 75 kWt, durante 24 Horas (entre las 06:00 y las 06:00 hrs. del día siguiente).',
            'En TODO Temuco y Padre Las Casas se prohíbe, durante 24 hrs (desde las 06:00 AM del día que se pronostique episodio, hasta las 06:00 AM del día siguiente), el funcionamiento de calderas con una potencia mayor a 75 kWt que presenten emisiones mayores o iguales a 50 mg/m³N de material particulado.',
            'IMPORTANTE Las estufas a pellet no poseen restricción de uso.'
        ]
    }

]


const selectStatus = (pm2_5: number) => {
    if (pm2_5 <= 50) return status[0]
    else if (pm2_5 <= 79) return status[1]
    else if (pm2_5 <= 109) return status[2]
    else if (pm2_5 <= 169) return status[3]
    else return status[4]
}

export default selectStatus;