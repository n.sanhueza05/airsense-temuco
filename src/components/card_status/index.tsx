import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import selectStatus from "./status";
import { ModalExtraInfo, ModalInformation } from "./components";

interface Props {
    pm2_5: number;
}

export default function CardStatus(props: Props) {

    const selectedStatus = selectStatus(props.pm2_5);


    return (
        <div className="card w-full bg-base-100 shadow-xl overflow-hidden">
            <div className="card-body grid grid-cols-2 pb-16">
                <h2 className="card-title">Estado Actual</h2>
                <div className="h-full flex items-center justify-end">
                    <ModalInformation color={selectedStatus.color} recommendations={selectedStatus.recommendations} textColor={selectedStatus.textColor} />
                </div>

                <div className="col-span-2 grid grid-cols-3">
                    <div className="col-span-1 h-full flex items-center">
                        <FontAwesomeIcon icon={selectedStatus.icon} color={selectedStatus.color} className="min-h-[100px] max-h-[100px]" />
                    </div>
                    <div className="col-span-1 h-full flex items-center justify-center lg:justify-normal">
                        <div>
                            <h3 className="md:text-lg">Actual</h3>
                            <h1 className="md:text-xl text-lg font-bold">{props.pm2_5}</h1>
                        </div>
                    </div>
                    <div className="col-span-1 h-full flex items-center">
                        <div className="-ms-6">
                            <h3 className="md:text-lg">Estado</h3>
                            <p className="md:text-xl text-lg font-bold">{selectedStatus.status}</p>
                        </div>
                    </div>
                </div>
                <div className="absolute bottom-5">
                    <ModalExtraInfo />
                </div>
            </div>
        </div>
    )
}