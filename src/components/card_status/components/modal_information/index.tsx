'use client'
import { faCircleInfo } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";

interface Props {
    textColor: string;
    color: string;
    recommendations: string[];
}

export default function ModalInformation(props: Props) {
    const [modalIsOpen, setmodalIsOpen] = useState(false);

    const openModal = () => setmodalIsOpen(true);
    const closeModal = () => setmodalIsOpen(false);

    return (
        <>
            <button className={props.textColor + ' flex items-center text-xs justify-center'} onClick={openModal}>
                Mas información
                <FontAwesomeIcon icon={faCircleInfo} color={props.color} className="max-h-[15px] ml-2" />
            </button>
            <dialog className={`modal ${modalIsOpen && 'modal-open'}`}>
                <form method="dialog" className="modal-box">
                    <h3 className="font-bold text-lg">Información Útil</h3>
                    <div className="overflow-x-auto">
                        <table className="table">
                            <tbody>
                                {props.recommendations.map((recommendation, index) => (
                                    <tr key={index}>
                                        <td>{recommendation}</td>
                                    </tr>
                                ))}
                            </tbody>
                        </table>
                    </div>
                    <div className="modal-action">
                        <button className="btn" onClick={closeModal} >Close</button>
                    </div>
                </form>
            </dialog>
        </>
    )
}