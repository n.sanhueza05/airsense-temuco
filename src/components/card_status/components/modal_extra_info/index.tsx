'use client'
import { faCircleInfo } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";

export default function ModalExtraInfo() {
    const [modalIsOpen, setmodalIsOpen] = useState(false);

    const openModal = () => setmodalIsOpen(true);
    const closeModal = () => setmodalIsOpen(false);
    return (
        <>
            <h3 className="text-sm hover:cursor-pointer" onClick={openModal}>Más información sobre los estados de calidad
                <FontAwesomeIcon icon={faCircleInfo} className="max-h-[15px] ml-2" />
            </h3>
            <dialog className={`modal ${modalIsOpen && 'modal-open'}`}>
                <form method="dialog" className="modal-box">
                    <h3 className="font-bold text-lg">Información calidad aire</h3>
                    <p>
                        Según las mediciones proporcionadas por el Ministerio del Medio Ambiente, se establecen los diferentes estados de calidad del aire en base a los siguientes rangos:
                    </p>
                    <div className="overflow-x-auto">
                        <table className="table">
                            <tbody>
                                <tr>
                                    <th>Buena<span className="badge badge-xs bg-text-good"></span></th>
                                    <td>0-50ug/m3</td>
                                </tr>
                                <tr>
                                    <th>Regular<span className="badge badge-xs bg-text-regular"></span></th>
                                    <td>51-79ug/m3</td>
                                </tr>
                                <tr>
                                    <th>Alerta<span className="badge badge-xs bg-text-alert"></span></th>
                                    <td>80-109ug/m3</td>
                                </tr>
                                <tr>
                                    <th>Preemergencia<span className="badge badge-xs bg-text-bad"></span></th>
                                    <td>110-169ug/m3</td>
                                </tr>
                                <tr>
                                    <th>Emergencia<span className="badge badge-xs bg-text-emergency"></span></th>
                                    <td>170ug/m3</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div className="modal-action">
                        <button className="btn" onClick={closeModal} >Close</button>
                    </div>
                </form>
            </dialog>
        </>
    )
}