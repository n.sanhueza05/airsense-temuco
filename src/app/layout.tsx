import './globals.css'
import { Inter } from 'next/font/google'

const inter = Inter({ subsets: ['latin'] })

export const metadata = {
  title: 'AirSense Temuco',
  description: 'Página web para ver la calidad del aire en Temuco',
}

export default function RootLayout({
  children,
}: {
  children: React.ReactNode
}) {
  return (
    <html lang="en">
      <body>
        <header className="navbar border flex justify-center items-center bg-air-primary text-white h-[100px]">
          <img src='/airsense_logo.png' className='h-full p-0 m-0' alt='logo airsense temuco' ></img>
          <h1 className='font-serif font-semibold text-xl'>Airsense Temuco</h1>
        </header>
        {children}
      </body>
    </html>
  )
}
