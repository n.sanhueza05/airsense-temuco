import { CardStatus, CardUV } from "@/components"
import CardRecommendations from "@/components/card_recommendations"
import ChartParticles from "@/components/chart_particles"
import transformData from "@/services/transform_data"

async function getData() {
  const uvRes = await fetch(`https://air-quality-api.open-meteo.com/v1/air-quality?latitude=-38.7396&longitude=-72.5984&hourly=uv_index&past_days=2&timezone=auto&domains=cams_global`, { cache: 'no-store' })
  const airRes = await fetch(`http://200.13.6.167:3031/api/v1/historicoDispositivo?dispositivo=0ec493a0-fbe1-11ec-9ef5-53c6b4ea55ee`,{ cache: 'no-store' })

  if (!uvRes.ok || !airRes.ok) {
    throw new Error('Failed to fetch data')
  }
  const uvData = await uvRes.json()
  const airData = await airRes.json()

  const data = transformData(uvData, airData)
  return data
}

export default async function Home() {
  const data = await getData()

  return (
    <main className="pt-5 grid grid-cols-6 xl:gap-y-0 gap-y-10 gap-x-10 xl:px-10">
      <article className="flex xl:col-span-2 lg:col-span-3 col-span-6 justify-center max-h-[300px]">
        <CardStatus pm2_5={data.currentPM2_5} />
      </article>
      <article className="flex xl:col-span-2 lg:col-span-3 col-span-6 justify-center max-h-[300px]">
        <CardUV currentUV={data.currentUV} />
      </article>
      <article className="flex xl:col-span-2 lg:col-span-3 col-span-6 justify-center max-h-[300px]">
        <CardRecommendations />
      </article>
      <article className="flex justify-center col-span-6 min-h-[500px] pt-10 w-full" >
        <div className="card w-full h-full bg-base-100 shadow-xl">
          <div className="card-body h-full">
            <ChartParticles dates={data.hours} pm10={data.pm10} pm2_5={data.pm2_5} uvs={data.uv} />
          </div>
        </div>
      </article>
    </main>
  )
}
