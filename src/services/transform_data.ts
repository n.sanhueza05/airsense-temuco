import { AirDataRes, AirResponseAPI, UVDataRes } from "@/types/custom";

export default async function transformData(uvData: UVDataRes, airData: AirDataRes): Promise<AirResponseAPI> {
  const currentPM2_5 = airData.dispositivo.historial.horas[24].mp2_5;

  const fechas: string[] = [];
  const mp2_5: number[] = [];
  const mp10: number[] = [];
  const concentracion: number[] = [];



  airData.dispositivo.historial.horas.forEach((hora) => {
    fechas.push(hora.fecha);
    mp2_5.push(hora.mp2_5);
    mp10.push(hora.mp10);
    concentracion.push(hora.concentracion);
  });

  const uvs = getUVs(uvData,fechas[24]);
  const currentUV = 0;
  const responseAPI: AirResponseAPI = {
    currentPM2_5: currentPM2_5,
    currentUV: currentUV,
    hours: fechas,
    pm2_5: mp2_5,
    pm10: mp10,
    uv: uvs
  }
  return responseAPI
}

function getUVs(uvData: UVDataRes, date : string) : number[] {
  const dateTransformed = transformDate(date);
  const getLastIndex = getLastIndexUV(uvData, dateTransformed);
  const uvArray = uvData.hourly.uv_index.slice(getLastIndex - 24, getLastIndex + 1);
  return uvArray
}

function transformDate(fechaOriginal: string): string {
  const fechaTransformada = new Date(fechaOriginal.replace(" ", "T"));
  const fechaFormateada = fechaTransformada.toISOString().replace("Z", "");

  return fechaFormateada;
}


function getLastIndexUV(uvData: UVDataRes, dateTransformed: string): number {
  const timeArray = uvData.hourly.time;

  for (let i = timeArray.length - 1; i >= 0; i--) {
    const fechaTransformada = transformDate(timeArray[i]);
    if (fechaTransformada === dateTransformed) {
      return i;
    }
  }

  throw new Error("No se ha encontrado la fecha");
}
