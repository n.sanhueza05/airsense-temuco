export interface AirDataRes {
    dispositivo: Dispositivo
}

export interface Dispositivo {
    id: string
    historial: Historial
}

export interface Historial {
    horas: Hora[]
    dias: Dia[]
}

export interface Hora {
    fecha: string
    ts: number
    mp2_5: number
    mp10: number
    temperatura: number
    humedad: number
    concentracion: number
}

export interface Dia {
    ts: number
    fecha: string
    concentracion_mp2_5: number
    concentracion_mp10: number
    mp2_5: number
    mp10: number
    temperatura: number
    humedad: number
}

export interface AirResponseAPI {
    currentPM2_5: number;
    currentUV: number;
    hours: string[];
    pm2_5: number[];
    pm10: number[];
    uv: number[];
}


export interface UVDataRes {
    latitude: number
    longitude: number
    generationtime_ms: number
    utc_offset_seconds: number
    timezone: string
    timezone_abbreviation: string
    hourly_units: HourlyUnits
    hourly: Hourly
}

export interface HourlyUnits {
    time: string
    uv_index: string
}

export interface Hourly {
    time: string[]
    uv_index: number[]
}

