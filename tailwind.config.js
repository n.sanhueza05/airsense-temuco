/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
    './src/components/**/*.{js,ts,jsx,tsx,mdx}',
    './src/app/**/*.{js,ts,jsx,tsx,mdx}',
  ],
  theme: {
    extend: {
      colors:{
        'air-primary':'#2F2F3D',
        'text-good':'#00A86B',
        'text-regular': '#FFC107',
        'text-alert': '#FF5722',
        'text-bad': '#D32F2F',
        'text-emergency': '#9C27B0'
      }
    }
  },
  plugins: [require("daisyui")],
  daisyui: {
    themes: false,
    darkTheme: false,
    base: false,
    styled:true,
    utils: true,
    logs: true,
  }
}